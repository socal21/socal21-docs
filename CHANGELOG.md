# CHANGELOG


## 2022

### 2022-02-03 Import der Schema-Datei mrh-lag.xsd
Die XML-Schema(XSD)-Datei wurde durch Reengineering erstellt, da es keine offizielle Version gibt.
Sie soll es erleichtern Exporte zur Metropolregion zu validieren.

Wir haben dabei einige Anpassungen wieder herausgenommen, die wir in unserem Projekt aus speziellen Erwägungen vorgenommen haben (wie das Entfernen von fax und phone1 als Elemente)

Änderungen

* Die IDs bei Category und Criterion werden (optional) zusätzliche ID-Elemente berücksichtigt. 
* AddressPoiType =3 als fixed gesetzt, da wir nur eventlocations ausspielen
* <cancelled> als boolean
* <freeOfCharge> als boolean
* Einige "mixed" Types entfernt
* Hinzufügen von Kommentaren
* Manche Reihenfolgen geändert
* Dokumentationen ergänzt
* Erste Version wurde erstellt aber noch nicht genutzt. Die Metropolregion nutzt bislang keine Schemata 
  zur Validierung  nicht




## 2021

### 2021-11-02 Viele Texte angepasst für die Veröffentlichung

### 2021-10-29 Neu aufsetzend er Dokumentation für Release

### GG2021-06-09 Repository von Github importiert
