---
title: "Kategorien"
date: 2021-09-01
latmod: 2021-11-04
draft: false
---

Die Kategorien beschreiben in aller Breite die verschiedensten Bereiche einer Veranstaltung. 
Teile der Definitionen wurden von und mit Genehmigung der Veranstaltungsdatenbank der Metropolregion Hamburg  übernommen und an unsere Gegebenheiten angepasst, da wir auch die gleiche Systematik verwenden. Daher zitieren wir diese hier (Urheber:innenrecht bei MRH[^1])


# Unsere Kategorien

## Konzert 

 * Country & Folk 
 * Elektro 
 * Gospel & Chöre 
 * HipHop, RnB & Soul 
 * Indie & Alternative 
 * Jazz, Blues, Swing & Chansons 
 * Klassik 
 * Pop 
 * Punkrock, Metal & Hardcore 
 * Reggae, Dancehall & Ska 
 * Rock 
 * Schlager & Volksmusik 
 * Singer/Songwriter 
 * Weltmusik 
 * Weitere Genres


## Festival 

Beschreibung: Kunst-, Kultur- und Musikfestival
Anwendungsbeispiele: Kurzfilmfestival, Langer Tag der Stadtnatur

## Party 

Beschreibung: Nachtleben, Disco

## Fest 

Anwendungsbeispiele: Erntedankfest, Sommerfest einer Schule, Hafengeburtstag

## Tanz

 * Tanzvorführung
 * Tanztreffen

## Theater 


## Weitere Bühnenkunst 

 * Comedy - Gewollt lustiges Kleinkunstprogramm auf einer Bühne
 * Kabarett - Kleinkunstprogramm mit Kritik an sozialen und/oder politischen Zuständen
 * Poetry Slam - Literarischer Vortragswettbewerb, bei dem selbstgeschriebene Texte innerhalb einer bestimmten Zeit einem Publikum vorgetragen werden. Die Zuhörer küren anschließend den Sieger
 * Musical
 * Zirkus
 * Bühnenkunst
 * Weitere Genres

## Film & Kino 

Beschreibung: Filmvorführungen

##  Ausstellung

Beschreibung: Temporäre Ausstellungen

 * Ausstellungseröffnung - Vernissage
 * Ausstellung

## Lesung 

Anwendungsbeispiele: Buchlesung

## Diskussion & Gespräch

Anwendungsbeispiele: Podiumsdiskussion

## Vortrag


## Konferenz


## Workshop


## Kurs & Seminar

## Gemeinschaftsaktivität

## Exkursion

 *   Wanderung
 *   Ausflug
 *   Stadtführung
 
## Markt

 *   Flohmarkt
 *   Handwerkermarkt
 *   Markt

## Kulinarische

Anwendungsbeispiele: Brunch




[^1]:  Texte der Metropolregion Hamburg
Die von der MRH übernommenen Sammlung der Beiträge sind urheberrechtlich geschützt. Weitere Informationen unter https://www.mrh.events/impressum.html

