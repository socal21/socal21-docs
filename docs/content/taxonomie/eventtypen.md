---
title: "Eventtypen"
date: 2021-10-01
lastmod: 2021-11-04
draft: false
categories: ["EventType", "Taxonomie"]
---

(beschreiben die Art des Events, maschinenlesbar )
Sind unten auf dieser Seite beschrieben: <https://schema.org/Event>

*   MusicEvent - https://schema.org/MusicEvent -  Musik
*   Festival - https://schema.org/Festival (selbsterklärend)
*   SocialEvent - https://schema.org/SocialEvent - Soziales
*   TheaterEvent - https://schema.org/TheaterEvent - Theater
*   ChildrensEvent - https://schema.org/ChildrensEvent - Kinder
*   ComedyEvent - https://schema.org/ComedyEvent - Comedy
*   VisualArtsEvent - https://schema.org/VisualArtsEvent - Bildende Kunst
*   ExhibitionEvent - https://schema.org/ExhibitionEvent - Ausstellung
*   LiteraryEvent - https://schema.org/LiteraryEvent - Literatur
*   SaleEvent - https://schema.org/SaleEvent - Verkauf
*   FoodEvent - https://schema.org/FoodEvent - Kulinarisches
*   EducationEvent - https://schema.org/EducationEvent - Bildung
*   ScreeningEvent - https://schema.org/ScreeningEvent - Kino
