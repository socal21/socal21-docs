---
title: "Merkmale"
date: 2021-09-01
lastmod: 2021-11-03
draft: false
---

# Was sind Merkmale?

Merkmale (oder Attribute)können vielfältig eingesetzt werden und bezeichnen zB Zielgruppen oder die Barrierfreiheit.

Wir haben einige Definition von und mit Genehmigung der **Veranstaltungsdatenbank der Metropolregion Hamburg**

# Unsere Merkmale

## Familien

Veranstaltungen, die primär auf Familien zugeschnitten sind
Nicht verwenden für: Veranstaltungen, die auch von Familien besucht werden können

## Ferienprogramm

Veranstaltungen die gezielt für Schulferien stattfinden.

## Jugendliche

Veranstaltungen, die primär auf Familien zugeschnitten sind

## Kinder

Veranstaltungen, die primär auf Familien zugeschnitten sind

## Kostenfrei

Die Veranstaltung ist auch ohne Eintrittsgeld zugänglich.

## Online-Veranstaltung

Für Veranstaltungen, die die Redaktion der Veranstaltungsdatenbank als besonders On-Demand-Angebot mit aufgenommen hat

## Open Air
 
Konzerte im Freien

## Plattdeutsch

Veranstaltungen in plattdeutscher Sprache oder über die plattdeutsche Sprache

## Queer

Veranstaltungen die vorrangig für die Zielgruppen Schwul, Lesbisch und Queer konzipiert sind

## Rollstuhlgerechte Sitzgelegenheiten 

Wenn der Hauptbereich ohne Stufen erreichbar ist und Rollstuhlfahrer ausreichend Platz haben, um sich zu bewegen und an einem Tisch zu sitzen. Wenn es nur hohe Tische (z. B. Stehtische) gibt, ist der Veranstaltungsort nicht rollstuhlgerecht.

## Rollstuhlgerechter Aufzug 

Wenn der Veranstaltungsort in einer Etage ist und es einen Aufzug gibt, der groß genug für einen Rollstuhl ist.

## Rollstuhlgerechter Eingang 

Wenn der Eingang ca. einen Meter breit ist und keine Stufen hat. Auf einem Meter können zwei Personen in der Regel gerade so bequem genau nebeneinander stehen. Falls eine oder mehrere Stufen vorhanden sind, sollte eine permanente Rampe installiert oder zumindest eine mobile Rampe verfügbar sein. Bei Eingängen, die lediglich eine Drehtür haben, kann dieses Merkmal nicht ausgewählt werden.

## Rollstuhlgerechtes WC

Wenn der Eingang zum WC mindestens einen Meter breit ist und keine Stufen aufweist. Wenn das WC einzelne Kabinen hat, müssen die Kabinentüren ebenfalls einen Meter breit sein. Ein Meter ist ungefähr so breit, dass zwei Personen bequem nebeneinander stehen können.

## Rollstuhlgerechter Parkplatz 

Wenn ein speziell ausgewiesener Behindertenparkplatz vorhanden ist. Diese sind oft mit Markierungen auf dem Boden oder mit Schildern versehen.


## Senioren

Veranstaltungen, die primär auf ältere Menschen zugeschnitten sind. Nicht verwenden für: Veranstaltungen, die auch von älteren Menschen besucht werden können

## Soliveranstaltung

Anwendungsbeispiele: Benefiz-Konzert, Solidaritätsveranstaltungen



[^1]: Fußnote
