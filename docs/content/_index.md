---
title: "SozioKulturKalender Dokumentation"
description: "Public API"
date: 2021-11-02
lastmod: 2022-02-07
draft: false
---

Auf dieser Seite informiert die [LAG Soziokultur Schleswig-Holstein](https://www.soziokultur-sh.de/) über ihren Veranstaltungskalenders "**SozioKulturKalender**". Das Projekt setzt auf international anerkannte Standards. Die Metadaten sind öffentlich frei zugänglich abrufbar. Die Prinzipien von Open Data und Open Access werden beachtet.


Die Entwicklung erfolgt auf Gitlab und kann dort verfolgt und unterstützt werden: https://gitlab.com/socal21/socal21-docs.

Diese Dokumentation auf diesen Seiten soll jeweils den aktuellen Stand beinhalten. 


Neue Entwicklungen und Änderungen findet ihr unter [Neues](/blog/)!

Dieses Projekt wurde aus den Mitteln der [Schleswig-Holsteinischen Landesbiliothek](https://www.schleswig-holstein.de/DE/Landesregierung/LBSH/lbsh_node.html) gefördert. Darüber hinaus wurde das Projekt umgesetzt mit [digiCULT Verbund eG](https://www.digicult-verbund.de/de) (Kiel) und der Firma [JUSTORANGE  - Agentur für Informationsästhetik](https://justorange.de) (Jena)


[Impressum](https://www.soziokultur-sh.de/impressum) -  [Datenschutzerklärung](https://www.soziokultur-sh.de/datenschutzerklaerung)
