---
title: "Neues"
date: 2021-10-31
lastmod: 2021-11-04
draft: false
---

In diesem Abschnitt kündigen wir jeweils neue Entwicklungen und Änderungen bei APIs und Software des "SozioKulturkalender" oder dieser Dokumentation an:
