---
title: "MRH XML Schema"
date: 2022-02-03
lastmod: 2022-02-10
draft: false
---

Wir haben ein XML Schema erstellt, mit dem wir unsere Export zur Metropolregion Hamburg/HHT erstellen. Näheres auf der [Seite zur MRH API](https://socal21.gitlab.io/socal21-docs/api/mrh/) (inklusive URL zur API)
