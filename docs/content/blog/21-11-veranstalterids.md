---
title: "Filter by_veranstalter_ids wird by_digiID_ids "
date: 2021-11-10
lastmod: 2021-11-10T08:39:25+01:00
draft: false
---


Im Bereich der [Veranstaltungen](/api/veranstaltungen) wurde der Filter by_veranstalter_ids in by_digiID_ids umbenannt. Die "digiID" kommt von digiCULT.xTree
