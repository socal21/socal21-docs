---
title: "English" 
description: "Public API"
date: 2021-11-02
lastmod: 2021-11-10
draft: false
---

On this page the AG Socioculture Schleswig-Holstein informs about the public [API](/api) for their project of a calendar of events of the socioculture. The project relies on internationally recognized standards and the metadata can be accessed in the public domain from servers that are freely accessible to the public.

Most documentation still is in [German](/de/). 

 * [Here a translation into english via Google Translate](https://socal21-gitlab-io.translate.goog/socal21-docs/de/?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=de&_x_tr_pto=nui)

The development can be followed and supported on Gitlab: https://gitlab.com/socal21/socal21-docs.


You can find new developments and changes under [News](/post/)!

This project was funded by the [Schleswig-Holsteinische Landesbiliothek](https://www.schleswig-holstein.de/DE/Landesregierung/LBSH/lbsh_node.html). In addition, the project was implemented with [digiCULT Verbund eG](https://www.digicult-verbund.de/de) (Kiel) and the company [JUSTORANGE - Agency for Information Aesthetics](https://justorange.de/) ( Jena) 
