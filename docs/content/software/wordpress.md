---
title: "Wordpress"
date: 2022-02-15
lastmod: 2022-02-15
draft: false
---

Für die Darstellung des Veranstaltungskalenders der neuen Website der LAG Soziokultur wurde ein Wordpress-Plugin von der Firma [PART GmbH für digitales Handeln](https://part.berlin/) (Berlin) programmiert.

* Lizenz: MIT, https://opensource.org/licenses/MIT
* Code Repository auf Gitlab: https://gitlab.com/partberlin/lag-soziokultur
* Mindestvoraussetzung: PHP 7.4.x

Damit können einerseits die Mitgliederporträts ausgespielt werden und zum anderen Veranstaltungen gefiltert nach Regionen, Kategorien usw.!

Der Plugin wird erst mit dem Release der Homepage zum Einsatz kommen.

