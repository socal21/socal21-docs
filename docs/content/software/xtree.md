---
title: "digiCULT.xTree"
date: 2021-11-30
lastmod: 2021-11-30
draft: false
keywords: ["xTree", "digiCULT"]
---

## Beschreibung
**xTree** ist eine webbasierte Anwendung für das kollaborative Erstellen und Pflegen kontrollierter Vokabulare (z.B.  Thesauri, Klassifikationen, Taxonomien). xTree ist semantisch
interoperabel mit [ISO 25964-1](https://www.iso.org/standard/53657.html).
Es ist über normale Browser (z.B. Firefox, Google Chrome, Microsoft Edge) verwendbar und wurde ausschließlich mit Open Source Software erstellt. 
Momentan verwaltet xTree mehr als 200 Vokabulare und Teilvokabulare, unter anderen den Thesaurus Wortnetz Kultur vom Landschaftsverband Rheinland, die LIDO-Terminologie, die Vokabulare der Deutschen Digitalen Bibliothek, das Vokabular für das
Graphikportal, den Thesaurus des Jüdischen Museums Berlin, die Objektbezeichnungsdatei der Landesstelle für die nichtstaatlichen Museen in Bayern und das von der LAG-Soziokultur verwendete CIT-Vokabular (Cultural Information Types).
Für den Zugriff auf die Vokabulare wird eine JSON-RESTful-Schnittstelle angeboten. Die einzelnen Vokabulare sind auch über RDF verfügbar. Einige Vokabulare wie die LIDO-Terminologie werden inzwischen über ein SPARQL/RDF-API angeboten.

* Website: https://www.digicult-verbund.de/de/

## Verwendung im Projekt

* Datenbank der Kultureinrichtungen für das Speichern der Mitglieder unseres Verbandes
* Taxonomien (CIT-Vokabular)

## Über digiCULT

digiCULT hat in Kooperation mit den Museumspartnern, unterstützt durch Fördermittel der Europäischen Union und des Landes Schleswig-Holstein, ein digitales Gesamtkonzept zur Dokumentation der Museumsbestände entwickelt. [mehr...](https://www.digicult-verbund.de/de/ueber-digicult)

Die LAG Soziokultur SH ist Mitglied der digiCULT eG.


