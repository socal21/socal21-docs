---
title: "Lizenz"
date: 2021-11-04
lastmod: 2021-11-04
draft: false
---


Die Dokumentationstexte sind, soweit nicht anders angegeben, gemeinfrei. Bzw. CC0 1.0 (Sprich: "CC Zero")
Siehe [Lizenztext](https://creativecommons.org/publicdomain/zero/1.0/deed.de).


Die Person, die ein Werk mit dieser Deed verknüpft hat, hat dieses Werk in die Gemeinfreiheit - auch genannt Public Domain - entlassen, indem sie weltweit auf alle urheberrechtlichen und verwandten Schutzrechte verzichtet hat, soweit das gesetzlich möglich ist.    Sie dürfen das Werk kopieren, verändern, verbreiten und aufführen, sogar zu kommerziellen Zwecken, ohne um weitere Erlaubnis bitten zu müssen. Siehe Sonstige Informationen unten. Diese Lizenz ist geeignet für freie kulturelle Werke.
## Sonstige Informationen {#customid}
In keiner Weise werden Patente oder Markenschutzrechte irgendeiner Partei von CC0 angetastet. Dasselbe gilt für Rechte, welche andere Personen am Werk oder an seiner Verwendung geltend machen können, wie etwa Öffentlichkeitsrechte oder Privatsphärenschutz.

Falls nicht anders angegeben, gibt die Person, die ein Werk mit dieser "Deed" verknüpft hat, keine Garantien hinsichtlich des Werks und übernimmt keinerlei Haftung für irgendwelche Nutzungen des Werks, soweit das gesetzlich möglich ist.

Wenn das Material genutzt oder zitiert wird, sollten Sie nicht den Eindruck einer Gutheißung erwecken durch den Rechteinhaber oder die Person, die das Werk identifiziert hat.

Einige Texte wurden mit Genehmigung der der **Veranstaltungsdatenbank der Metropolregion Hamburg**(MRH) ([Quelle](https://www.mrh.events/hilfe/info/merkmale.html) ) übernommen und an unsere Gegebenheiten angepasst, da wir auch die gleiche Systematik verwenden. Daher zitieren wir diese hier (Urheber:innenrecht bei MRH, [^1])´:


[^1]: Texte der Metropolregion Hamburg
Die von der MRH übernommenen Sammlung der Beiträge sind urheberrechtlich geschützt. Weitere Informationen unter https://www.mrh.events/impressum.html
