---
title: "Schema.org Veranstaltungen"
date: 2021-09-01
lastmod: 2022-03-09
suchbereich: "performance"
draft:  false
categories: ["Taxonomie", "Veranstaltungen"]
tags:
- JSON-LD
- Schema.org
- 
weight: 2
---

Die Veranstaltungen benutzen primär https://schema.org/Event als Grundlage.

- URL zum Abruf Beispiel: {{< param apiurl >}}api?apiKey={{< param apikey >}}&search={{< param suchbereich >}}&type=2328&style=json-ld

Der Beginn der Datei sieht wie folgt aus:

```json
 [ {                                  
 "@context":"https://schema.org/", 
 "@language":"DE",                 
 "@type":"Event",                  
 "..."
},
```


## Filter

Filterparameter für das Filtern via URL. Diese werden also an die bestehende URL angehängt. Die Reihenfolge ist dabei egal. 

### Nach Änderungsdatum

- `&start_ch_date=[YYYY-mm-dd]` _z.B.: 2021-09-30 Filterung nach Datum. (dieser Tag inbegriffen)_
- `&end_ch_date=[YYYY-mm-dd]` _z.B.: 2021-09-30 Filterung vor diesem Datum. (dieser Tag inbegriffen)_

### Nach der Parent-ID der Veranstaltung

-  `&by_ids[]=[Zahl]` _z.B.: &by_ids[]=2&by_ids[]=3_ - Suche nach der (darüberliegenden) ID des Events. Es können mehrere IDs eingegeben werden. Parent-IDs sind vor allem bei Tourneen , Ausstellungen oder Veranstaltungsreihen als gemeinsame Klammer verschiedener der Einzelveranstaltung wichtig. Eine Parent ID kann mehrere  "Spielzeit_ids" haben.

### Nach der ID der/des Veranstalters/Veranstalterin (LAG-Mitglieder)
-   `&by_digiID_ids[]=[Zahl]` _z.B.: &by_digiID_ids[]=1_ - Ähnlich wie by_ids, bloß Mitglieder/Veranstalter können eingegeben werden. Die digiID wird aus digiCULT.xTree bezogen.

### Nach der ID der Veranstaltung

-   `&by_spielzeit_ids[]=[Zahl]` _z.B.: &by_spielzeit_ids[]=1 siehe by_digiID_ids_ . Wichtig zu beachten ist, dass entgegen des Namens, es hier nicht um "Spielzeiten" geht. Die Benennung soll in Zukunft geändert werden.

### Nach der Anfangszeit/datum der Veranstaltung
-   `&start_date=[YYYY-mm-dd]` _z.B.: &start_date=2020-09-30_ Filterung nach Datum -   startDate - Die Anfangszeit verwendet den Standard [ISO 8601](https://de.wikipedia.org/wiki/ISO_8601). „2021-10-26T10:00:00+01:00"" also Datum, Uhrzeit und Zeitzonenabweichung in einem String.
### Nach dem Ende der Veranstaltung
-   `&end_date=[YYYY-mm-dd]` _z.B.:&end_date=2020-12-30_ - endDate - Die Endzeit verwendet den Standard [ISO 8601](https://de.wikipedia.org/wiki/ISO_8601) „2021-10-26T10:00:00+01:00"" also Datum, Uhrzeit und Zeitzonenabweichung in einem String.

### Nach Postleitzahl

* `&plz=`

### Nach Bundesland

* `&bundesland=`

### Nach Landkreisen (und kreisfreien Städten)

* `&landkreis=`

### Nach Ortsnamen

* `&ort=`


## Verwendete Knotennamen

-   eventAttendanceMode -- zB
    (<https://schema.org/OfflineEventAttendanceMode>,
    <https://schema.org/OnlineEventAttendanceMode>,
    <https://schema.org/MixedEventAttendanceMode> ) (= Offline, Online,
    Mixed) Mixed entspricht „hybride" Veranstaltung teils online, teils
    offline.
-   eventStatus 
    - Standard ist <https://schema.org/EventScheduled> (der Termin findet statt)
    - oder der Termin wurde Abgesagt:  <https://schema.org/EventCancelled>). Der Grund der Absage, falls angegeben, kann gefunden werden unter:
	- identifier (name ="cancelReason" und als value der Ausfallgrund) 
-   DateModified -- Datum der letzten Änderung diese Veranstaltungsobjektes (für Updates)
-   duration - die Dauer ist ein optionales Feld
-   doorTime - sind (optionale) Einlasszeiten vor dem Beginn der Veranstaltung <https://schema.org/doorTime> .
-   description ist die Text/Veranstaltungsbeschreibung
-   name - Veranstaltungstitel
-   @language: Momentan taucht hier nur „DE" ([ISO 3166](https://de.wikipedia.org/wiki/ISO_3166)) auf. 
    Dänisch. 
-   about - Kurzbeschreibung 
-   identifier --  Identifier nach dem Linkdata-Standard zu xTree
-   Organizer -- Mitgliedsname (sind auch die Veranstalter:innen)
    -   PropertyValue/identifier/ID  - zb "act0002484"(entspricht einer digiCULT-ID in xTree)
-   Kategorien splitten sich auf in
	-   PropertyValue „Kategorie" für Veranstaltungsseite
		-   identifier 
		-   PropertyValue „Kategorie Primary - primäre Kategorie
		-   PropertyValue Suchkriterien -- zentrale Suchkriterien für die LAG-Website!
		-   Merkmale (= Icons bei digiCULT -- sowas wie Parkplatz, Barrierefreheit usw.) siehe unter [Taxonomie/Merkmale](/taxonomie/merkmale)
    -   PropertyValue Parent-ID -- gemeinsame ID für Veranstaltungen, die zusammen gehören (zB mehrtägige Veranstaltungen oder Tourneen!) 
-   url -- Link zum Event
-   image ImageObject -- Veranstaltungsfoto/s 
    -   caption: Untertitel
    -   creator: Fotograf:innenname 
    -   description: Beschreibung 
    -   copyrightNotice: Verwendungshinweis  
    -   license: Lizenztext 
-   location address -- Veranstaltungsort
-   location geo -- Geokoordinaten
    -   latitude
    -   longitude
-   offers -- Buchungslink
-   performer -- das sind zB die Künstler:innen. Erst mal nur als
    strukturierte Daten als JSON-LD ausspielen. Ggf. passenden Ort auf
    der Seite finden. Wir zur Zeit nicht befüllt. 
-   @type event -- zeigt die Art des Events zusätzlich zu den
    Kategorien, Merkmalen usw. Soll in den strukturierten Daten
    auftauchen,  (zB [MusicEvent](https://schema.org/MusicEvent), ComedyEvent,Festival, DanceEvent, LiteraryEvent,
    ChildrensEvent, FoodEvent, ScreeningEvent, TheaterEvent,...) (siehe
    unter <https://schema.org/Event> „More specific Types"
-   BusStation - NAH SH/ Anschluss öffentliche Verkehrsmittel 
-   maximumAttendeeCapacity -- Maximale Teilnehmendenzahl (ignorieren)
-   MaximumPhysicalAttendeeCapacity -- Maximale Teilnehmehmende vor Ort
    (ignorieren)
-   maximumVirtualAttendeeCapacity . Maximale Teilnehmende virtuell
    (ignorieren)

