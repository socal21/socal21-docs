---
title: "Schema.org Veranstaltungs API"
date: 2021-09-01
lastmod: 2022-03-09
draft: false
tags:
- JSON-LD
- Schema.org
- API
weight: 1
---

Unsere native API basiert auf Schema.org (und JSON-LD). 
## Vorteile

- Schema.org ist weltweit als Standard anerkannt und wird von Suchmaschinenfirmen wie Google oder Yahoo unterstützt
- Es gibt ein definiertes Vokabular, dass eindeutig ist und es erlaubt auch bereits ohen Dokumentation die autoamtische Erkennung der Daten
- Es gibt bereits eine Vielzahl von Austauschstandards. Diese erschweren mittlerweile aber eher den Austausch als dass sie ih erleichtern
- Dadurch, dass der Aufbau der Daten von vorne herein klar war, konnten wir Zeit sparen bei der Entwicklung
- Erweiterungen und Änderungen des Standards sind möglich, ohne das man notwendiger weise die Funktionalität zerstört.
- Es ist zu erwarten, dass es vermehrt Werkzeuge gibt, die das Lesen und Schreiben des Standards erleichtern
- Es gibt bereits eine Dokumentation des Standards, wir mussten diesen lediglich erweitern.
- Der Standard scheint zukunftssicher zu sein. Und wir können davon profitieren, wenn es in Zukunft neue Definitionen gibt.
- Der Standard ist nicht branchenspezifisch
- Als bekannter Standard werden immer mehr Programmierende, Firmen udn Organisationen die Syntax kennen und können sofort damit arbeiten, anstatt sich erst "reinzudenken"

## Nachteile

- Einfachere APIs sind denkbar. Abruf und Import sind aufwendiger als bei simplen APIs
- Für unsere Anwendungsfälle gab es nicht immer bereits Begriffe, aber das Vokabular ist erweiterbar


## Definition 
**Schema.org** ist eine Initiative, die eine einheitliche Ontologie für
die Strukturierung von Daten auf Websites auf der Basis von bereits
bestehenden Auszeichnungssprachen entwickelt.

-   Schema.org Dokumentationen: <https://schema.org/docs/documents.html>
-   Validator:
    [https://validator.schema.org](https://validator.schema.org/)


## Unsere Schemata

Wir verwenden natürlich nicht alle möglichen Schemata, sondern lediglich eine Auswahl. Wir haben zwei Suchbereiche als Zugriff über die URL: **veranstaltungen** und **mitglieder**.

-   **Event** https://schema.org/Event - im Suchbereich [Veranstaltungen](/api/veranstaltungen)
-   **Place, LocalBusiness** - im Suchbereich [Mitglieder](/api/mitglieder)
-   Person
-   Offer
-   **Organization** https://schema.org/Organization

(fett gedruckt und verlinkt die Wichtigsten)
