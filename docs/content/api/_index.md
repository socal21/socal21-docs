---
title: "APIs"
date: 2021-09-01
lastmod: 2021-12-16
draft: false
weight: 3
tags:
- Schema.org
- API
---

[ [Google translated english](https://socal21-gitlab-io.translate.goog/socal21-docs/api/?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=de&_x_tr_pto=nui) ]

## Einführung 


### Öffentliche API (Schema.org)
Die Öffentliche API von SoCal21 dient dem Datenaustausch von Termin und Adressdaten über JSON. Die Basis stellt dabei JSON-LD und spezifischer auf [Schema.org](https://schema.org) dar. Dabei wurden nur bestimmte Teile der Spezifikation verwendet.


#### Konventionen
-   Umlaute werden in UTF-8 ausgegeben
-   HTML Entitäten ( &quot; können vorkommen)
-   Zeilenumbrüche werden so markiert: \\r\\n\\r\\n
-   Einfaches **Markdown** kann vorkommen (zB: \*" am
    Zeilen Anfang, Headings (\# und \#\#) als Überschriften,
	 \*\*bold\*\* und \_\_italic\_\_.


#### Grund-URL

`{{<param apiurl >}}?type=2328&style=json-ld&search=[suchbereich]&apiKey=[apiKey]`

Der öffentliche API-Key lautet: {{< param apikey >}}

* Test-URL für Veranstaltungen: {{<param apiurl >}}?apiKey={{< param apikey >}}search=performance&type=2328&style=json-ld

#### Die Suchbereiche

Es gibt zwei Streams:

 1. [Veranstaltungen](./veranstaltungen) mit Suchbereich "performance"
 2. [Mitglieder](./mitglieder) der LAG Soziokultur mit Suchbereich "mitglieder"


### Weitere APIs

* [API der Metropolregion Hamburg](./mrh)

