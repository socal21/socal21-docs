---
title: "Schema.org: Mitglieder"
date: 2021-09-01
lastmod: 2021-11-04
suchbereich: "mitglieder"
draft: false
categories: ["Taxonomie"]
tags:
- Schema.org
- JSON-LD
- Adressen
- Mitglieder

weight: 3
---

Ausgabe der Mitglieder der LAG Schleswig-Holstein.

Die Veranstaltungen benutzen primär https://schema.org/LocalBusiness als Grundlage.

- URL zum Abruf Beispiel: {{< param apiurl >}}api?apiKey={{< param apikey >}}&search={{< param suchbereich >}}&type=2328&style=json-ld

```json
 [ {                                  
 "@context":"https://schema.org/", 
 "@language":"DE",                 
 "@type":"LocalBusiness",                  
 "..."
},
```

## Filter

Filterparameter für das Filtern via URL. Diese werden also an die bestehende URL angehängt. Die Reihenfolge ist dabei egal.

### Nach ID der Organisation

Die IDs kommen von digiCULT (digiID)

* `&id=` z.B.:`&id=act0002402` zeigt nur die Daten von "Aktion Jugendzentrum Neumünster e.V. (AJZ)" (act0002402)

### Nur Mitglieder der LAG Soziokultur

* `&lag_members=1` listet nur die Mitglieder der LAG Soziokultur 


## Verwendete Knotennamen
- @context "https://www.schema.org/" Definiert das Ganze als Schema.org kompatibel
- @type "localBusiness" --  https://schema.org/LocalBusiness
- name -- Der Name der Organisation https://schema.org/name
- alternateName -- alternativer Name oder Kürzel (optional)  https://schema.org/alternateName (Kann den Raumnamen enthalten hinter dem "$"!)
- identifier -- verschiedene IDs, digiID 
    - ID /value -- Die digiCULT ID als eindeutiger Identifier. Diese wird aus der Datenbank [digiCULT.xTree](https://www.digicult-verbund.de/de/digicultxtree) entnommen
    - Lizenz -- die Lizenz des Datensatzes
- address / PostalAddress (post) -- Besucheradresse (visitor)
    -   streetAddress - Straße
    -   addressLocality -- Ort
    -   postalCode - Postleitzahl
    -   addressCountry -- 2 Buchstaben [ISO 3166](https://de.wikipedia.org/wiki/ISO_3166)
    -   PropertyValue
    -   location \@type Place
    -   geo (longitute und latitude) -- in WGS84 Standard
- location (Geolocation)
-   URI underConstruction (ignorieren)
-   BusStation -- https://schema.org/BusStation - Informationen zum ÖPNV 
-   email -- Mitglieds-Maildresse
-   url -- Mitglieds-Website
-   ImageObject -- Fotos vom Mitglied
    -   contentUrl, creator, caption, description, keywords
-   Merkmale (= Icons bei digiCULT -- sowas wie Parkplatz, Barrierefreheit usw.) siehe unter [Taxonomie/Merkmale](/taxonomie/merkmale)
-   description -- Längere Beschreibung
-   disambiguationDescription/0 -- Teaser
    -   /1 -- Kurzbeschreibung 

