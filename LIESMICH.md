# LIESMICH

Dies ist das Repository für die Dokumentation zum Projekt eines Veranstaltungskalenders für den Verband "Landesarbeitgemeinschaft (LAG) Soziokultur in Schleswig-Holstein e.V."

Das Projekt bietet eine Schnittstelle (API) basierend auf [Schema.org](https://schema.org)

## Inhalte

* Verzeichnis "[docs](./docs)" beinhaltet die Website, die auf "hugo" basiert
* '[Verschiedene Texte](./Verschiedene Texte)' ist eben das. Ein Sammelbecken für Texte, die ganz oder teilweise in die Website als Dokumentation einfließen.


Bei Fragen zu dem Projekt und der Dokumentation kontaktieren sie bitte: admin@soziokultur-sh.de
